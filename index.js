
'use strict'

class Handy{

    constructor(){
        this.zoll;
        this.farbe;
        this.marke;
        this.preis;
    }
    
    ausgabetexterstellen(){
        return "Bilschirmdiagonale: " + this.zoll + "Zoll" + "<br>" + "Marke: " + this.marke + "<br>" + "Farbe: " +this.farbe + "<br>";
    }
} 

let handy = new Handy();

function btnSetHandyOnClick() {
    handy.zoll = document.getElementById('tbxZoll').value;
    handy.farbe = document.getElementById('tbxFarbe').value;
    handy.marke = document.getElementById('tbxMarke').value;
    document.getElementById("lblAusgabe").innerHTML = "Bildschirmdiagonale: " + handy.zoll +" " +"Zoll <br>" + "Marke: "+ handy.marke + "<br> Farbe: " + handy.farbe ;
    db.run("INSERT INTO handyTabelle (marke, zoll, farbe) VALUES (?,?,?);", [handy.marke,handy.zoll,handy.farbe]);
    window.localStorage.setItem("handyDatenbank2", datenbankAufFestplatteSchreiben(db.export()));    
    console.log(handy);
}

function btnUpdateOnClick(){
    db.run("UPDATE handyTabelle SET marke = ?, zoll = ? WHERE farbe = ?;", [handy.marke, handy.zoll, handy.farbe]);
}
var sql = require('./sql.js');
var db;

if(localStorage.getItem("handyDatenbank2") == null){

    console.log("Es existiert noch keine Datenbank. Sie wird jetzt erstellt ...");

    db = new sql.Database();

    db.run("CREATE TABLE IF NOT EXISTS handyTabelle (marke char, zoll int, farbe char);");
}

else{ 
    
    console.log("Die existierende Datenbank wird geöffnet ...") 
    
    db = new sql.Database(datenbankVonFestplatteLaden(localStorage.getItem("handyDatenbank2")));
}

function btnAlleHandysAnzeigenOnClick(){
    var abfrageErgebnis = db.prepare("SELECT * FROM handyTabelle");
    let handyListe = [];

    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject();
        let handy = new Handy();
        handy.marke = row.marke;
        handy.zoll = row.zoll;
        handy.farbe = row.farbe;
        handyListe.push(handy);
    }
    console.log(handyListe);
    console.log("Die Farbe des ersten Handys wird ausgegeben: " + handyListe[0].farbe);

    let ausgabetext = "";

    for(let i = 0; i <handyListe.length; i++){
        console.log(handyListe[i]);
        ausgabetext += handyListe[i].ausgabetexterstellen() + "<br>";
    }
    document.getElementById("lblAusgabe").innerHTML = ausgabetext;
}
console.log("Die Datenbank wird auf die Festplatte geschrieben ...");    
window.localStorage.setItem("handyDatenbank2", datenbankAufFestplatteSchreiben(db.export()));    

function datenbankAufFestplatteSchreiben (arr) {
    var uarr = new Uint8Array(arr);
    var strings = [], chunksize = 0xffff;

    for (var i=0; i*chunksize < uarr.length; i++){
        strings.push(String.fromCharCode.apply(null, uarr.subarray(i*chunksize, (i+1)*chunksize)));
    }    
    return strings.join('');
}

function datenbankVonFestplatteLaden (str) {
    var l = str.length,
            arr = new Uint8Array(l);
    for (var i=0; i<l; i++) arr[i] = str.charCodeAt(i);    
    return arr;
}
function btnAlleLoeschenOnClick(){
    db.run("DELETE FROM handyTabelle");
    document.getElementById("lblAusgabe").innerHTML = "Alle Handys gelöscht";
    console.log("Die Datenbank wird auf die Festplatte geschrieben ...");
    window.localStorage.setItem("handyDatenbank2", datenbankAufFestplatteSchreiben(db.export()));
}